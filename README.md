# EP2 - OO (UnB - Gama)

Este projeto consiste em uma aplicação desktop para um restaurante, utilizando a técnologia Java Swing.

# Status da aplicação:

A aplicação não foi finalizada, a interface foi implementada via java swing e está disponivel no repositorio a ligação da interface com a parte de controle e modelagem não foi feita!

# Como executar a interface:

-Baixe ou clone o repositorio do projeto em seu computador;

-Abra o projeto atraves da IDE netbeans;

-Execute a classe TelaPrincipal;

-Navege pelos menus;

-Imagine a aplicação funcionando;